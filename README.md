# Game of Life

Conway's Game of Life for the Teensy 3.6 microcontroller and an LED matrix.

The goal of this project is to create a piece of dynamic 'art' of the geeky variety. With that in mind, there are end conditions in place to 'reset' the board to keep the algorithm from becoming completely static.

## Bill of Materials

* Adafruit's 32 x 32 RGB LED Matrix Panel - 5mm Pitch (Adafruit #2026)
* SmartMatrix SmartLED Shield (V4) for Teensy 3 (Adafruit #1902) https://github.com/pixelmatix/SmartMatrix
* Teensy 3.6 without header
* 5V 4A switching power supply (Adafruit #1609)
* Various random electronics parts
  * .1 pitch headers for the Teensy

## Libraries Needed

* The Teensyduino libraries https://www.pjrc.com/teensy/teensyduino.html for programming the Teensy via the Arduino IDE
* The SmartMatrix libraries

## Features

## References

* Include a link to the sparkfun/adafruit projects on how to wire up the panel, etc.

## TODO

* Include links to BOM
* Include photos/gifs
* Include wiring diagrams, if needed

### Software

* Implement loop detection for X amount of ticks
* Figure out how to walk the neighbors to make sure that color is the same
* Analyze the runtime for memory / time speedups
* Create a tripwire that resets the game after `x` ticks of the game clock to prevent long running loops.

### Hardware

* Figure out a frame for the panel
  * Include the micro-usb to panel mount header
  * Include a power plug
* Figure out how to power the Teensy from the external power supply
