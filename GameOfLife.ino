#include <SmartLEDShieldV4.h>
#include <SmartMatrix3.h>

#define COLOR_DEPTH 24                  
const uint8_t kMatrixWidth = 32;        
const uint8_t kMatrixHeight = 32;       
const uint8_t kRefreshDepth = 36;       
const uint8_t kDmaBufferRows = 4;       
const uint8_t kPanelType = SMARTMATRIX_HUB75_32ROW_MOD16SCAN; 
const uint8_t kMatrixOptions = (SMARTMATRIX_OPTIONS_NONE);      // see http://docs.pixelmatix.com/SmartMatrix for options
const uint8_t kBackgroundLayerOptions = (SM_BACKGROUND_OPTIONS_NONE);

SMARTMATRIX_ALLOCATE_BUFFERS(matrix, kMatrixWidth, kMatrixHeight, kRefreshDepth, kDmaBufferRows, kPanelType, kMatrixOptions);
SMARTMATRIX_ALLOCATE_BACKGROUND_LAYER(backgroundLayer, kMatrixWidth, kMatrixHeight, COLOR_DEPTH, kBackgroundLayerOptions);

//const int defaultBrightness = (100*255)/100;    // full (100%) brightness
const int defaultBrightness = (15*255)/100;    // dim: 15% brightness
const int defaultScrollOffset = 6;

const rgb24 black = {0x00, 0, 0};
const rgb24 white = {0xff, 0xff, 0xff};

// Loop speed in milliseconds
const uint FPS = 1000/4;



// Teensy 3.0 has the LED on pin 13
const int ledPin = 13;

// Game State:
rgb24 board[kMatrixHeight][kMatrixWidth];
rgb24 next[kMatrixHeight][kMatrixWidth];

uint births = 0;
uint deaths = 0;

rgb24 cell = white;

rgb24 selectedColor = black;

void setup() {
  // initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);
  Serial.begin(38400);
  randomSeed(analogRead(0));
  
  matrix.addLayer(&backgroundLayer); 
  matrix.begin();

  matrix.setBrightness(defaultBrightness);
  backgroundLayer.enableColorCorrection(true);
  

  setupBoard();
}

void setupBoard() {
  for( int i = 0; i < kMatrixHeight; i++ ) {
    for( int j = 0; j < kMatrixWidth; j++ ) {
      int rand = random(0,2);
      if( rand > 0 ) {
        cell = generateRandomColor();
      } else {
        cell = black;
      }
      board[i][j] = cell;
      next[i][j] = board[i][j];
    }
  }
  cell = white;
}

/*
 * Determine if the cell located at the listed row,col
 * is empty or not. Also sets the selectedColor variable.
 * Returns 0 if the cell is empty, or if the row,col is greater than
 * the limits. Returns 1 if the cell is filled.
 */
int checkCell( int row, int col ) {
  if( row >= 0 && row < kMatrixWidth && col >= 0 && col < kMatrixHeight ) {
    if( !equalColor(black, board[row][col])) {
      // Change the color of the selectedColor if the color is either
      // black or randomly. This is just to keep the new cells from being
      // the same color/pattern.
      int rand = random(0,2);
      if( equalColor(black, selectedColor) || rand > 0 ) {
        selectedColor = board[row][col];
      } 
      
      return 1;
    }
  }
  return 0;
}

/*
 * Iterate all of the neighbors of the cell.
 * Return the number of neighbors found.
 */
int determineNeighbors(int i, int j) {
  int result = 0;

  for( int col = -1; col < 2; col++ ) {
    for( int row = -1; row < 2; row++ ) {
      if( !(row == 0 && col == 0) ) {
          result += checkCell(i+col,j+row);
      }
    }
  }

  return result;
}

void loop() {
  uint currentDeaths = 0;
  uint currentBirths = 0;

  // Setup the next board state
  for( int i = 0; i < kMatrixWidth; i++ ) {
    for( int j = 0; j < kMatrixHeight; j++ ) {
      selectedColor = black;
      int neighbors = determineNeighbors(i, j);
  
      if( equalColor( black, board[i][j])) {
        // Dead Cell
        if( neighbors == 3 ) {
          next[i][j] = selectedColor;
  
          currentBirths++;
        }
      } else {
        // Live Cell
        if( neighbors == 2 || neighbors == 3 ) {
          // Staying alive
          next[i][j] = board[i][j];
        } else {
          // Ack! They've got me!
          next[i][j] = black;
          currentDeaths++;
        }
      }
    }
  }

  displayBoard();

  // Check for loops / end condition
  if( currentDeaths == deaths && currentBirths == births ) {
    stepColor();
  } else {
    deaths = currentDeaths;
    births = currentBirths;  
  }

  // Slow down the ticks
  delay(FPS); 
}

// Swap the board with the new state and paint the pixels.
// In theory, should be able to reduce this to a pointer swap...
void displayBoard() {
  // swap the boards with the new state
  for( int i = 0; i < kMatrixWidth; i++ ) {
    for( int j = 0; j < kMatrixHeight; j++ ) {
      board[i][j] = next[i][j];
    }
  }
      
  // paint the pixels
  for( int i = 0; i < kMatrixWidth; i++ ) {
      for( int j = 0; j < kMatrixHeight; j++ ) {
        backgroundLayer.drawPixel(i, j, board[i][j]);
      }
  }
  backgroundLayer.swapBuffers();
}

// Determine if two rgb24 colors are the same or not.
boolean equalColor( rgb24 op1, rgb24 op2 ) {
  return op1.red == op2.red 
      && op1.blue == op2.blue 
      && op1.green == op2.green;
}

// Generate and return a random color. Will not return black (as that represents
// a dead cell) and instead will return (0x01,0x01,0x01).
rgb24 generateRandomColor() {
  rgb24 result;
  result.red = random(0,256);
  result.blue = random(0,256);
  result.green = random(0,256);
  // Check to make sure we didn't accidently create black. 
  // If so, bump up the color by 1 on each hue
  if( equalColor( black, result ) ) {
    result.red = 0x1;
    result.green = 0x1;
    result.blue = 0x1;
  }
  
  return result;
}

// In theory, steps through all the colors before finally resetting the game state.
void stepColor() {
  cell = stepColor(cell);
  if( equalColor( black, cell)) {
    // Done stepping down.
    setupBoard();
  } else {
    boolean allBlack = true;
    for( int i = 0; i < kMatrixWidth; i++ ) {
      for( int j = 0; j < kMatrixHeight; j++ ) {
        board[i][j] = stepColor(board[i][j]);
        allBlack = allBlack && equalColor( black, board[i][j]);
      }
    }    
    if( allBlack ) {
      setupBoard();
    }
  }
}

rgb24 stepColor(rgb24 color ) {
  if( equalColor( black, color )) {
    return black;
  }
  color.red = color.red != 0x00 ? color.red - 0x1 : 0x00;
  color.blue = color.blue != 0x00 ? color.blue - 0x1 : 0x00;
  color.green = color.green != 0x00 ? color.green - 0x1 : 0x00;
  return color;
}
